package activitybasic.itcast.cn.heima.demo01_shengmingzhouqi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import activitybasic.itcast.cn.heima.R;

public class Main2Activity extends AppCompatActivity {

    // Activity 被创建时调用
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Log.i("Activity02", "onCreate()");
    }

    // Activity 可见时调用
    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Activity02", "onStart()");
    }

    // Activity 获取焦点时调用
    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Activity02", "onResume()");
    }

    // Activity 失去焦点时调用
    @Override
    protected void onPause() {
        super.onPause();
        Log.i("Activity02", "onPause()");
    }

    // Activity 不可见时调用
    @Override
    protected void onStop() {
        super.onStop();
        Log.i("Activity02", "onStop()");
    }

    // Activity 被销毁时调用
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Activity02", "onDestroy()");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("Activity02", "onRestart()");
    }

}
