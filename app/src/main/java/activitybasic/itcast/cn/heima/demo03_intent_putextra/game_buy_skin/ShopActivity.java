package activitybasic.itcast.cn.heima.demo03_intent_putextra.game_buy_skin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import activitybasic.itcast.cn.heima.R;
import activitybasic.itcast.cn.heima.demo03_intent_putextra.game_buy_skin.domain.ItemInfo;

public class ShopActivity extends AppCompatActivity implements View.OnClickListener {

    private ItemInfo itemInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        //创建页面同时赋属性值
        itemInfo=new ItemInfo("金剑", 100, 20, 20);

        findViewById(R.id.rl).setOnClickListener(this);//全屏可以点击

        TextView mLifeTv=(TextView) findViewById(R.id.tv_life);
        TextView mNameTv=(TextView) findViewById(R.id.tv_name);
        TextView mSpeedTv=(TextView) findViewById(R.id.tv_speed);
        TextView mAttackTv=(TextView) findViewById(R.id.tv_attack);
        //TextView显示字符串，这里传入int值编译不会报错，运行会报错。
        mLifeTv.setText("生命值+" + itemInfo.getLife());
        mNameTv.setText(itemInfo.getName());
        mSpeedTv.setText("敏捷度+" + itemInfo.getSpeed());
        mAttackTv.setText("攻击力+" + itemInfo.getAcctack());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl:
                Intent intent=new Intent();
                intent.putExtra("equipment", itemInfo);//传递数据对象
                setResult(1, intent);//让activity返回他的回调者
                finish();
                break;
        }
    }
}
