package activitybasic.itcast.cn.heima.demo03_intent_putextra.registered_user_data;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import activitybasic.itcast.cn.heima.R;

public class Main42Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main42);

        //获取 Intent对象
        Intent intent=getIntent();
        //取出key对应的value值
        String name =intent.getStringExtra("name");
        String password=intent.getStringExtra("password");
        String sex=intent.getStringExtra("sex");

        TextView tv_name=(TextView) findViewById(R.id.tv_name);
        TextView tv_password=(TextView) findViewById(R.id.tv_password);
        TextView tv_sex=(TextView)  findViewById(R.id.tv_sex);

        tv_name.setText("用户名："+name);
        tv_password.setText("密  码："+password);
        tv_sex.setText("性  别："+sex);
    }
}
