package activitybasic.itcast.cn.heima.demo14_Video.suefaceview;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import activitybasic.itcast.cn.heima.R;

public class Main22Activity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, SurfaceHolder.Callback {

    private SurfaceView sv;
    private SurfaceHolder holder;
    private MediaPlayer mediaplayer;
    private int position;
    private RelativeLayout rl;
    private Timer timer;
    private TimerTask task;
    private SeekBar sbar;
    private ImageView play;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.activity_main22);
        ////////////////////
        sbar=(SeekBar) findViewById(R.id.sbar);
        play=(ImageView) findViewById(R.id.play);
        sbar.setOnSeekBarChangeListener(this);
        sv=(SurfaceView) findViewById(R.id.sv);
        /////////////////////
        timer=new Timer();//jdk中提供的一个定时器工具子线程执行指定的计划任务
        task=new TimerTask() {//实现了Runnable接口的抽象类，是可以被Timer执行的任务
            @Override
            public void run() {
                if (mediaplayer != null && mediaplayer.isPlaying()) {
                    int progress=mediaplayer.getCurrentPosition();
                    int total=mediaplayer.getDuration();
                    sbar.setMax(total);
                    sbar.setProgress(progress);
                }
            }
        };
        timer.schedule(task, 500, 500);//设置延迟500毫秒，每隔500ms执行一次
        /////////////////////////////
        rl=(RelativeLayout) findViewById(R.id.re_11);
        holder=sv.getHolder();
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);//4.0以上不必写
        holder.addCallback(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (rl.getVisibility() == View.INVISIBLE) {
                    rl.setVisibility(View.VISIBLE);
                    // 倒计时3秒
                    CountDownTimer cdt=new CountDownTimer(3000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            // 每隔1秒执行一次这个方法
                            Log.e("onTouchEvent", "onTick");
                        }

                        @Override
                        public void onFinish() {
                            // 每隔3秒执行一次这个方法
                            rl.setVisibility(View.INVISIBLE);
                        }
                    };
                    cdt.start();//执行倒计时方法
                } else if (rl.getVisibility() == View.VISIBLE) {
                    rl.setVisibility(View.INVISIBLE);
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        timer.cancel();
        task.cancel();
        timer=null;
        task=null;
        super.onDestroy();
    }

    public void click(View view) {
        if (mediaplayer != null && mediaplayer.isPlaying()) {
            mediaplayer.pause();
            play.setImageResource(android.R.drawable.ic_media_play);
        } else {
            mediaplayer.start();
            play.setImageResource(android.R.drawable.ic_media_pause);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        //进度发生变化时出发
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        //进度条开始拖动时触发
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        //进度条拖动停止时出发
        int position=seekBar.getProgress();
        if (mediaplayer != null && mediaplayer.isPlaying()) {
            mediaplayer.seekTo(position);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //SurfaceHolder创建完时触发
        try {
            mediaplayer=new MediaPlayer();
            mediaplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            // 没有资源会异常
            mediaplayer.setDataSource("/storage/emulated/0/Download/v.mp4");
            mediaplayer.setDisplay(holder);
            mediaplayer.prepareAsync();
            mediaplayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaplayer.start();
                    if (position>0){
                        mediaplayer.seekTo(position);
                    }
                }
            });
        } catch (IOException e) {
            Toast.makeText(Main22Activity.this,"播放失败",Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //SurfaceHolder大小变化时触发
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //SurfaceHolder注销时触发
        position=mediaplayer.getCurrentPosition();//记录上次播放的位置，然后停止
        mediaplayer.stop();
        mediaplayer.release();
        mediaplayer=null;
    }
}
