package activitybasic.itcast.cn.heima.demo08_BroadcastReceiver.orderedbroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyBroadcastReceiver03 extends BroadcastReceiver {
    public MyBroadcastReceiver03() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("MyBroadcastReceiver03", "03接收到广播");
    }
}
