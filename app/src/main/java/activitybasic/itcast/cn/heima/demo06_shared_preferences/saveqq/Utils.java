package activitybasic.itcast.cn.heima.demo06_shared_preferences.saveqq;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * Ceate author: qihao on 2016/10/20.15:42
 * (位置：) activitybasic.itcast.cn.heima.demo06_shared_preferences.saveqq (Context)Utils
 * TODO->主要功能：存取sp
 * 邮箱：sin2t@sina.com
 */
public class Utils {
    //保存QQ号码和登陆密码到data.xml文件中
    public static boolean saveUserInfo(Context context,String number,String password){
        SharedPreferences sp=context.getSharedPreferences("data",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();
        editor.putString("userName",number);
        editor.putString("pwd",password);
        editor.commit();
        return true;
    }
    //从data.xml文件中获取存储的QQ号码和密码
    public static Map<String,String> getUserInfo(Context context){
        SharedPreferences sp=context.getSharedPreferences("data",Context.MODE_PRIVATE);
        String number =sp.getString("userName",null);
        String password =sp.getString("pwd",null);
        Map<String,String> userMap=new HashMap<String,String>();
        userMap.put("number",number);
        userMap.put("password",password);
        return userMap;
    }
}
