package activitybasic.itcast.cn.heima.demo01_shengmingzhouqi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import activitybasic.itcast.cn.heima.R;

public class MainActivity extends AppCompatActivity {

    // Activity 被创建时调用
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("Activity01", "onCreate()");
    }

    // Activity 可见时调用
    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Activity01", "onStart()");
    }

    // Activity 获取焦点时调用
    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Activity01", "onResume()");
    }

    // Activity 失去焦点时调用
    @Override
    protected void onPause() {
        super.onPause();
        Log.i("Activity01", "onPause()");
    }

    // Activity 不可见时调用
    @Override
    protected void onStop() {
        super.onStop();
        Log.i("Activity01", "onStop()");
    }

    // Activity 被销毁时调用
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Activity01", "onDestroy()");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("Activity01", "onRestart()");
    }

    // 界面中按钮的点击事件
    public void click(View view) {
        //创建一个Intent对象，通过该对象开启第二个Activity.
        Intent intent=new Intent(MainActivity.this,Main2Activity.class);
        startActivity(intent);
    }
}
