package activitybasic.itcast.cn.heima.demo08_BroadcastReceiver.broadcast;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import activitybasic.itcast.cn.heima.R;

public class Main12Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main12);
    }

    public void send(View view) {
        Intent intent=new Intent();
        // 定义广播事件的类型
        intent.setAction("www.itcast.cn");
        // 发送广播
        sendBroadcast(intent);
    }
}
