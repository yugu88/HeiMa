package activitybasic.itcast.cn.heima.demo04_xml.xml_weather;

/**
 * Created by qihao on 2016/10/20.10:34
 * for: HeiMa.
 */

public class WeatherInfo {
    private int id;
    private String name;
    private String weather;
    private String temp;
    private String pm;
    private String wind;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id=id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name=name;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather=weather;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp=temp;
    }

    public String getPm() {
        return pm;
    }

    public void setPm(String pm) {
        this.pm=pm;
    }

    public String getWind() {
        return wind;
    }

    public void setWind(String wind) {
        this.wind=wind;
    }
}
