package activitybasic.itcast.cn.heima.demo08_BroadcastReceiver.orderedbroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyBroadcastReceiver01 extends BroadcastReceiver {
    public MyBroadcastReceiver01() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("MyBroadcastReceiver01", "01接收到广播");
        abortBroadcast();//在此优先级最高的广播接收者中拦截了广播
        Log.i("MyBroadcastReceiver01", "01拦截了广播");
    }
}
