package activitybasic.itcast.cn.heima.demo13_animation.animation2;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import activitybasic.itcast.cn.heima.R;

public class Main20Activity extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_flower;
    private Button btn_start;
    private AnimationDrawable animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main20);

        iv_flower = (ImageView) findViewById(R.id.iv_flower);
        btn_start = (Button) findViewById(R.id.btn_play);
        btn_start.setOnClickListener(this);
        //拿到AnimationDrawable对象
        animation = (AnimationDrawable)iv_flower.getBackground();
    }

    @Override
    public void onClick(View v) {
        if (!animation.isRunning()){
            animation.start();
            btn_start.setBackgroundResource(android.R.drawable.ic_media_pause);
        }else {
            animation.stop();
            btn_start.setBackgroundResource(android.R.drawable.ic_media_play);
        }
    }
}
