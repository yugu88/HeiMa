package activitybasic.itcast.cn.heima.demo03_intent_putextra.registered_user_data;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import activitybasic.itcast.cn.heima.R;

public class Main4Activity extends AppCompatActivity {

    private EditText et_name;
    private EditText et_password;
    private Button btn_send;
    private RadioButton manRadio;
    private RadioButton womanRadio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        et_name=(EditText) findViewById(R.id.et_name);
        et_password=(EditText) findViewById(R.id.et_password);
        btn_send=(Button) findViewById(R.id.btn_send);
        manRadio=(RadioButton) findViewById(R.id.radio_male);
        womanRadio=(RadioButton) findViewById(R.id.radio_female);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passData();
            }
        });
    }

    private void passData() {
        //创建Intent对象，启动其他activity
        Intent intent=new Intent(Main4Activity.this,Main42Activity.class);
        intent.putExtra("name",et_name.getText().toString().trim());
        intent.putExtra("password",et_password.getText().toString().trim());
        String str="";
        if (manRadio.isChecked()){
            str="男";
        }else if (womanRadio.isChecked()){
            str="女";
        }
        intent.putExtra("sex",str);
        startActivity(intent);
    }
}
