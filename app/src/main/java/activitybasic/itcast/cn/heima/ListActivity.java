package activitybasic.itcast.cn.heima;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import activitybasic.itcast.cn.heima.demo01_shengmingzhouqi.Main2Activity;
import activitybasic.itcast.cn.heima.demo01_shengmingzhouqi.MainActivity;
import activitybasic.itcast.cn.heima.demo02_opencamera.opencamera.Main3Activity;
import activitybasic.itcast.cn.heima.demo03_intent_putextra.game_buy_skin.Main5Activity;
import activitybasic.itcast.cn.heima.demo03_intent_putextra.registered_user_data.Main4Activity;
import activitybasic.itcast.cn.heima.demo04_xml.xml_weather.Main8Activity;
import activitybasic.itcast.cn.heima.demo05_fileinput_output.readandwrite.Main6Activity;
import activitybasic.itcast.cn.heima.demo06_shared_preferences.saveqq.Main9Activity;
import activitybasic.itcast.cn.heima.demo07_db_goods.product.Main10Activity;
import activitybasic.itcast.cn.heima.demo08_BroadcastReceiver.broadcast.Main12Activity;
import activitybasic.itcast.cn.heima.demo08_BroadcastReceiver.ipdail.Main11Activity;
import activitybasic.itcast.cn.heima.demo08_BroadcastReceiver.orderedbroadcast.Main13Activity;
import activitybasic.itcast.cn.heima.demo09_service.bindservice.Main15Activity;
import activitybasic.itcast.cn.heima.demo09_service.startservice.Main14Activity;
import activitybasic.itcast.cn.heima.demo10_service_example.musicplayer.Main16Activity;
import activitybasic.itcast.cn.heima.demo11_HttpURLConnection.imageview.Main17Activity;
import activitybasic.itcast.cn.heima.demo12_Canvas.guagua.Main18Activity;
import activitybasic.itcast.cn.heima.demo13_animation.animation.Main19Activity;
import activitybasic.itcast.cn.heima.demo13_animation.animation2.Main20Activity;
import activitybasic.itcast.cn.heima.demo14_Video.suefaceview.Main22Activity;
import activitybasic.itcast.cn.heima.demo14_Video.videoview.Main21Activity;
import activitybasic.itcast.cn.heima.demo15_sensor.shake.ShakeActivity;

public class ListActivity extends Activity {

    private RecyclerView gridView;
    private List<String> date = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        gridView = (RecyclerView) findViewById(R.id.grid_view);
        String[] name = {"生命周期", "生命周期2", "隐式意图之相机", "Intent跳转传值", "activity跳转之买皮肤",
                "openFile存取", "天气解析XML", "SharedPreferences存取", "数据库增删改查", "接收广播消息存本地",
                "发送广播", "有序方式发送同一广播", "startService", "bindService", "bindService控制音乐服务",
                "HttpURLConnection解析bitmap", "刮刮乐", "属性动画", "帧动画", "videoView", "SurfaceView组合mediaplayer",
                "摇一摇"};
        for (int i = 0; i < name.length; i++) {
            date.add(name[i]);
        }
        gridView.setLayoutManager(new GridLayoutManager(this, 2));
        gridView.setHasFixedSize(true);
        gridView.setItemAnimator(new DefaultItemAnimator());
        RecyclerListAdapter adapter = new RecyclerListAdapter(date, this);
        gridView.setAdapter(adapter);
        adapter.setOnItemClickLitener(new RecyclerListAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent();
                switch (position) {
                    case 0:
                        intent.setClass(ListActivity.this, MainActivity.class);
                        break;
                    case 1:
                        intent.setClass(ListActivity.this, Main2Activity.class);
                        break;
                    case 2:
                        intent.setClass(ListActivity.this, Main3Activity.class);
                        break;
                    case 3:
                        intent.setClass(ListActivity.this, Main4Activity.class);
                        break;
                    case 4:
                        intent.setClass(ListActivity.this, Main5Activity.class);
                        break;
                    case 5:
                        intent.setClass(ListActivity.this, Main6Activity.class);
                        break;
                    case 6:
                        intent.setClass(ListActivity.this, Main8Activity.class);
                        break;
                    case 7:
                        intent.setClass(ListActivity.this, Main9Activity.class);
                        break;
                    case 8:
                        intent.setClass(ListActivity.this, Main10Activity.class);
                        break;
                    case 9:
                        intent.setClass(ListActivity.this, Main11Activity.class);
                        break;
                    case 10:
                        intent.setClass(ListActivity.this, Main12Activity.class);
                        break;
                    case 11:
                        intent.setClass(ListActivity.this, Main13Activity.class);
                        break;
                    case 12:
                        intent.setClass(ListActivity.this, Main14Activity.class);
                        break;
                    case 13:
                        intent.setClass(ListActivity.this, Main15Activity.class);
                        break;
                    case 14:
                        intent.setClass(ListActivity.this, Main16Activity.class);
                        break;
                    case 15:
                        intent.setClass(ListActivity.this, Main17Activity.class);
                        break;
                    case 16:
                        intent.setClass(ListActivity.this, Main18Activity.class);
                        break;
                    case 17:
                        intent.setClass(ListActivity.this, Main19Activity.class);
                        break;
                    case 18:
                        intent.setClass(ListActivity.this, Main20Activity.class);
                        break;
                    case 19:
                        intent.setClass(ListActivity.this, Main21Activity.class);
                        break;
                    case 20:
                        intent.setClass(ListActivity.this, Main22Activity.class);
                        break;
                    case 21:
                        intent.setClass(ListActivity.this, ShakeActivity.class);
                        break;

                }
                startActivity(intent);

            }
        });
    }

}
