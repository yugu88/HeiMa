package activitybasic.itcast.cn.heima.demo13_animation.animation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import activitybasic.itcast.cn.heima.R;

public class Main19Activity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img01;
    private ImageView img02;
    private ImageView img03;
    private ImageView img04;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main19);

        img01 = (ImageView) findViewById(R.id.img01);
        img02 = (ImageView) findViewById(R.id.img02);
        img03 = (ImageView) findViewById(R.id.img03);
        img04 = (ImageView) findViewById(R.id.img04);
        img01.setOnClickListener(this);
        img02.setOnClickListener(this);
        img03.setOnClickListener(this);
        img04.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img01:
                //调用AnimationUtils的loadAnimation加载动画
                Animation ani1=AnimationUtils.loadAnimation(this,R.anim.alpha_animation);
                img01.startAnimation(ani1);
                break;
            case R.id.img02:
                Animation ani2=AnimationUtils.loadAnimation(this,R.anim.scale_animation);
                img02.startAnimation(ani2);
                break;
            case R.id.img03:
                Animation ani3=AnimationUtils.loadAnimation(this,R.anim.translate_animation);
                img03.startAnimation(ani3);
                break;
            case R.id.img04:
                Animation ani4=AnimationUtils.loadAnimation(this,R.anim.rotate_animation);
                img04.startAnimation(ani4);
                break;
        }
    }
}
