package activitybasic.itcast.cn.heima.demo04_xml.xml_serialize;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Xml;
import android.view.View;
import android.widget.Toast;

import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import activitybasic.itcast.cn.heima.R;

public class Main7Activity extends AppCompatActivity {

    private List<PersonInfo> userData;//保存数据集合

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);

        userData=new ArrayList<PersonInfo>();
        for (int i=0; i < 3; i++) {
            userData.add(new PersonInfo("王" + i, 100 - i, 80 - i));
        }
    }

    public void Serializer(View view) {
        try {

            XmlSerializer serializer=Xml.newSerializer();
            File file=new File(Environment.getExternalStorageDirectory(), "person.xml");
            FileOutputStream os=new FileOutputStream(file);
            serializer.setOutput(os, "UTF-8");
            serializer.startDocument("UTF-8", true);
            serializer.startTag(null, "persons");
            int count=0;
            for (PersonInfo person : userData) {
                serializer.startTag(null,"person");
                serializer.attribute(null,"id",count+"");
                //将Person对象的name属性写入XML
                serializer.startTag(null,"name");
                serializer.text(person.getName());
                serializer.endTag(null,"name");
                //将Person对象的age属性写入XML
                serializer.startTag(null,"age");
                serializer.text(String.valueOf(person.getAge()));
                serializer.endTag(null,"age");
                //将Person对象的score属性写入XML
                serializer.startTag(null,"score");
                serializer.text(String.valueOf(person.getScore()));
                serializer.endTag(null,"score");
                serializer.endTag(null,"person");
                count++;
            }
            serializer.endTag(null,"persons");
            serializer.endDocument();
            serializer.flush();
            os.close();
            Toast.makeText(Main7Activity.this,"操作成功",Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
