package activitybasic.itcast.cn.heima.demo10_service_example.musicplayer;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.File;

import activitybasic.itcast.cn.heima.R;

public class Main16Activity extends AppCompatActivity implements View.OnClickListener {

    private EditText mPath;
    private Intent mIntent;
    private myConn mConn;
    MediaService.MyBinder mBinder;
    private SeekBar mSeekBar;
    private Thread mThread;

    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 100:
                    int currentPosition=(Integer)msg.obj;
                    mSeekBar.setProgress(currentPosition);
                    break;
                default:
                    break;
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main16);

        mPath=(EditText) findViewById(R.id.et_inputpath);
        findViewById(R.id.bt_play).setOnClickListener(this);
        findViewById(R.id.bt_pause).setOnClickListener(this);
        findViewById(R.id.bt_replay).setOnClickListener(this);
        findViewById(R.id.bt_stop).setOnClickListener(this);
        mSeekBar=(SeekBar) findViewById(R.id.seekbar1);
        mConn=new myConn();//绑定服务
        mIntent=new Intent(Main16Activity.this,MediaService.class);
        bindService(mIntent,mConn,BIND_AUTO_CREATE);

    }

    @Override
    public void onClick(View v) {
        // 音乐文件放入模拟器或者真机的 mnt/sdcard目录下
        String pathway=mPath.getText().toString().trim();
        File SDpath=Environment.getExternalStorageDirectory();
        File file=new File(SDpath,pathway);
        String path=file.getAbsolutePath();
        switch (v.getId()){
            case R.id.bt_play:
                if (file.exists()&&file.length()>0){
                    mBinder.plays(path);
                    initSeekBar();
                    UpdateProgress();
                }else {
                    Toast.makeText(Main16Activity.this,"找不到音乐文件",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.bt_pause:
                mBinder.pauses();
                break;
            case R.id.bt_replay:
                mBinder.replays(pathway);
                break;
            case R.id.bt_stop:
                mThread.interrupt();// 退出子线程
                if (mThread.isInterrupted()){
                    mBinder.stops();
                }
                break;
            default:
                break;
        }
    }

    private void UpdateProgress() {
        mThread=new Thread(){
            @Override
            public void run() {
                while (!interrupted()){
                    int currentPosition=mBinder.getCurrentPosition();
                    Message message=Message.obtain();
                    message.obj=currentPosition;
                    message.what=100;
                    handler.sendMessage(message);
                }
            }
        };
        mThread.start();
    }

    private void initSeekBar() {
        int musicWidth=mBinder.getMusicWidth();//获取音乐文件的长度
        mSeekBar.setMax(musicWidth);
    }

    private class myConn implements ServiceConnection{

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBinder=(MediaService.MyBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    }

    @Override
    protected void onDestroy() {
//        if (mThread!=null&!mThread.isInterrupted()){
//            mThread.interrupt();
//        }
        unbindService(mConn);//必须先停止子线程才能解绑服务，否则异常
        super.onDestroy();
    }
}
