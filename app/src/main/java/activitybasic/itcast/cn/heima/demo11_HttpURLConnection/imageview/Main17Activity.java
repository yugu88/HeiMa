package activitybasic.itcast.cn.heima.demo11_HttpURLConnection.imageview;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import activitybasic.itcast.cn.heima.R;

public class Main17Activity extends AppCompatActivity {

    protected static final int CHANGE_UI=1;
    protected static final int ERROR=2;
    private EditText et_path;
    private ImageView iv;

    private Handler handler=new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == CHANGE_UI) {
                Bitmap bitmap=(Bitmap) msg.obj;
                iv.setImageBitmap(bitmap);
            } else if (msg.what == ERROR) {
                Toast.makeText(Main17Activity.this, "显示图片", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main17);

        et_path=(EditText) findViewById(R.id.et_path);
        iv=(ImageView) findViewById(R.id.iv);
    }

    public void click(View view) {
        //final String path=et_path.getText().toString().trim();
        final String path="http://p2.so.qhmsg.com/bdr/_240_/t016531700e8fb9f3bc.jpg";
        if (TextUtils.isEmpty(path)) {
            Toast.makeText(Main17Activity.this, "图片路径不能为空", Toast.LENGTH_SHORT).show();
        } else {
            new Thread() {
                private HttpURLConnection conn;
                private Bitmap bitmap;

                @Override
                public void run() {
                    try {
                        URL url=new URL(path);
                        conn=(HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        conn.setConnectTimeout(5000);
                        conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; " +
                                "SV1; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; " +
                                ".NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; Shuame)");
                        int code=conn.getResponseCode();
                        if (code==200){
                            InputStream is=conn.getInputStream();
                            bitmap=BitmapFactory.decodeStream(is);
                            Message msg=new Message();
                            msg.what=CHANGE_UI;
                            msg.obj=bitmap;
                            handler.sendMessage(msg);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Message msg=new Message();
                        msg.what=ERROR;
                        handler.sendMessage(msg);
                    }
                }
            }.start();
        }
    }
}
