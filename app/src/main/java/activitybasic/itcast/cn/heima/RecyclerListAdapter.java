package activitybasic.itcast.cn.heima;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by xiaoqiang on 2017/7/19 9:56
 * 2017 to: 邮箱：sin2t@sina.com
 * androidApp2
 */

public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.myHolde> {
    private List<String> date;
    private Context context;

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);
        //void onItemLongClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
    //-------------------------------------------------
    public RecyclerListAdapter(List<String> date, Context context) {
        this.date = date;
        this.context = context;
    }

    @Override
    public RecyclerListAdapter.myHolde onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.item_grid, null);
        return new myHolde(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerListAdapter.myHolde holder, int position) {

        holder.textView.setText(date.get(position));
        if (mOnItemClickLitener != null) {
            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getLayoutPosition();
                    mOnItemClickLitener.onItemClick(holder.textView, pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.date.size();
    }

    class myHolde extends RecyclerView.ViewHolder {

        private final TextView textView;

        public myHolde(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text_grid);
        }
    }
}
