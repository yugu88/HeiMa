package activitybasic.itcast.cn.heima.demo08_BroadcastReceiver.orderedbroadcast;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import activitybasic.itcast.cn.heima.R;
/**
 * Ceate author: qihao on 2017/7/11 19:31
 * (位置：) activitybasic.itcast.cn.heima.demo08_BroadcastReceiver.orderedbroadcast (Context)Main13Activity
 * TODO->主要功能：发送有序广播
 * 邮箱：sin2t@sina.com
 */
public class Main13Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main13);
    }

    public void send(View view) {
        Intent intent=new Intent();
        // 定义广播事件的类型
        intent.setAction("www.itcast.cn");
        // 发送有序广播
        sendOrderedBroadcast(intent,null);
    }
}
