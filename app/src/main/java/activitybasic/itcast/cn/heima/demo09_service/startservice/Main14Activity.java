package activitybasic.itcast.cn.heima.demo09_service.startservice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import activitybasic.itcast.cn.heima.R;

/**
 * Ceate author: qihao on 2017/7/11 19:39
 * (位置：) activitybasic.itcast.cn.heima.demo09_service.startservice (Context)Main14Activity
 * TODO->主要功能：Service
 * 邮箱：sin2t@sina.com
 */
public class Main14Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main14);
    }

    public void start(View view) {
        Intent intent=new Intent(Main14Activity.this,MyService.class);
        startService(intent);
    }

    public void stop(View view) {
        Intent intent=new Intent(Main14Activity.this,MyService.class);
        stopService(intent);
    }
}
