package activitybasic.itcast.cn.heima.demo03_intent_putextra.game_buy_skin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import activitybasic.itcast.cn.heima.R;
import activitybasic.itcast.cn.heima.demo03_intent_putextra.game_buy_skin.domain.ItemInfo;

public class Main5Activity extends AppCompatActivity {

    private TextView mLifeTV;
    private TextView mAttackTV;
    private TextView mSpeedTV;
    private ProgressBar mProgressBar1;
    private ProgressBar mProgressBar2;
    private ProgressBar mProgressBar3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        mLifeTV=(TextView) findViewById(R.id.tv_life_progress);
        mAttackTV=(TextView) findViewById(R.id.tv_attack_progress);
        mSpeedTV=(TextView) findViewById(R.id.tv_speed_progress);
        initProgress();
    }

    private void initProgress() {
        mProgressBar1=(ProgressBar) findViewById(R.id.progress_bar1);
        mProgressBar2=(ProgressBar) findViewById(R.id.progress_bar2);
        mProgressBar3=(ProgressBar) findViewById(R.id.progress_bar3);

        mProgressBar1.setMax(1000);
        mProgressBar2.setMax(1000);
        mProgressBar3.setMax(1000);

    }

    //开启新的activity并获取返回值
    public void click(View view) {
        Intent intent=new Intent(Main5Activity.this, ShopActivity.class);
        startActivityForResult(intent, 1);//返回请求结果，请求码是1
    }

    public void click2(View view) {
        Intent intent=new Intent(Main5Activity.this, ShopActivity.class);
        startActivityForResult(intent, 1);//返回请求结果，请求码是1
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (resultCode == 1) { //确认是ShopActivity传回来的intent
                if (requestCode == 1) { //确认相应的是click2()方法发送的请求吗
                    ItemInfo info=(ItemInfo) data.getSerializableExtra("equipment");
                    //更新ProgressBar 的值
                    updateProgress(info);
                }
            }
        }
    }

    private void updateProgress(ItemInfo info) {
        int progress1=mProgressBar1.getProgress();
        int progress2=mProgressBar2.getProgress();
        int progress3=mProgressBar3.getProgress();

        mProgressBar1.setProgress(progress1 + info.getLife());
        mProgressBar2.setProgress(progress2 + info.getAcctack());
        mProgressBar3.setProgress(progress3 + info.getSpeed());

        mLifeTV.setText(mProgressBar1.getProgress() + "");
        mAttackTV.setText(mProgressBar2.getProgress() + "");
        mSpeedTV.setText(mProgressBar3.getProgress() + "");
    }
}
