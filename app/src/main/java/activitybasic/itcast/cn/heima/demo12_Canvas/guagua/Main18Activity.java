package activitybasic.itcast.cn.heima.demo12_Canvas.guagua;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import activitybasic.itcast.cn.heima.R;

public class Main18Activity extends AppCompatActivity {

    private ImageView mImageView;
    private Bitmap alterbitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main18);
        ///////////////////////
        mImageView=(ImageView) findViewById(R.id.imgv);
        Bitmap bitmap=BitmapFactory.decodeResource(getResources(), R.mipmap.guagua);
        alterbitmap=Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());

        final Canvas canvas=new Canvas(alterbitmap);
        Paint paint=new Paint();
        paint.setColor(Color.BLACK);
        paint.setAntiAlias(true);
        Matrix matrix=new Matrix();
        canvas.drawBitmap(bitmap, matrix, paint);
        mImageView.setImageBitmap(alterbitmap);
        mImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Toast.makeText(Main18Activity.this, "手指触下", Toast.LENGTH_SHORT).show();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        Log.i("手指移动===", "手指移动（" + event.getX() + "," + event.getY() + ")");
                        int y=(int) event.getY();
                        int x=(int) event.getX();
                        for (int i=-10; i < 10; i++) {
                            for (int j=-10; j < 10; j++) {
                                int xx=x + i;
                                int yy=y + j;
                                if (xx >= 0&&xx<mImageView.getWidth()&&yy>=0&&yy<mImageView.getHeight()) {
                                    Log.i("手指坐标：",xx+":"+yy);
                                    alterbitmap.setPixel(xx, yy, Color.TRANSPARENT);
                                }
                            }
                        }
                        mImageView.setImageBitmap(alterbitmap);
                        break;
                    case MotionEvent.ACTION_UP:
                        Toast.makeText(Main18Activity.this, "手指松开", Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });
    }
}
