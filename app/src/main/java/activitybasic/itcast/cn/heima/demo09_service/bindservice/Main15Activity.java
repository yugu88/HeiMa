package activitybasic.itcast.cn.heima.demo09_service.bindservice;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import activitybasic.itcast.cn.heima.R;

public class Main15Activity extends AppCompatActivity {

    private MyService.MyBinder myBinder;
    private MyConn myconn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main15);
    }

    public void btn_bind(View view) {
        if (myconn == null) {
            myconn=new MyConn();
        }
        Intent intent=new Intent(this, MyService.class);
        bindService(intent,myconn,BIND_AUTO_CREATE);
    }

    public void btn_call(View view) {
        myBinder.callMethodInService();
    }

    public void btn_unbind(View view) {
        if (myconn != null) {
            unbindService(myconn);
            myconn=null;
            myBinder=null;
        }
    }
    private class MyConn implements ServiceConnection{

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {//绑定服务成功
            //MyBinder是服务中继承Binder的内部类
            myBinder=(MyService.MyBinder)service;
            Log.i("Main15Activity","成功绑定，内存地址为："+myBinder.toString());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {//解除绑定

        }
    }
}
