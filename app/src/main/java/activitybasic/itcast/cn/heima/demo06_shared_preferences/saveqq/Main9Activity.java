package activitybasic.itcast.cn.heima.demo06_shared_preferences.saveqq;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Map;

import activitybasic.itcast.cn.heima.R;

public class Main9Activity extends AppCompatActivity implements View.OnClickListener {

    private EditText etNumber;
    private EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main9);

        initView();
        //取出号码
        Map<String,String> userInfo=Utils.getUserInfo(this);
        if (userInfo!=null){
            //显示
            etNumber.setText(userInfo.get("number"));
            etPassword.setText(userInfo.get("password"));
        }
    }

    private void initView() {
        etNumber=(EditText)findViewById(R.id.et_number);
        etPassword=(EditText)findViewById(R.id.et_password);
        findViewById(R.id.btn_login).setOnClickListener(this );
    }

    @Override
    public void onClick(View v) {
        //当单机“登录”按钮时，获取QQ号码和密码
        String number=etNumber.getText().toString().trim();
        String password=etPassword.getText().toString().trim();
        //校验号码和密码是否正确
        if (TextUtils.isEmpty(number)){
            Toast.makeText(Main9Activity.this,"请输入QQ账号",Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(password)){
            Toast.makeText(Main9Activity.this,"请输入密码",Toast.LENGTH_SHORT).show();
            return;
        }
        //登录成功
        Toast.makeText(Main9Activity.this,"登录成功",Toast.LENGTH_SHORT).show();
        //如果正确，判断是否勾选了记住密码
        Log.i("Main9Activity","记住密码："+number+","+password);
        //保存用户信息
        boolean isSaveSuccess=Utils.saveUserInfo(Main9Activity.this,number,password);
        if (isSaveSuccess){
            Toast.makeText(Main9Activity.this,"保存成功",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(Main9Activity.this,"保存失败",Toast.LENGTH_SHORT).show();
        }
    }
}
