package activitybasic.itcast.cn.heima.demo09_service.bindservice;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {

    class MyBinder extends Binder{
        public void callMethodInService(){
            methodInService();
        }
    }

    @Override
    public void onCreate() {
        Log.i("MyService","onCreate()");
        super.onCreate();
    }

    private void methodInService() {
        Log.i("MyService","methodInService()");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i("MyService","onBind()");
        return new MyBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i("MyService","onUnbind()");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.i("MyService","onDestroy()");
        super.onDestroy();
    }
}
