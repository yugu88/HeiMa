package activitybasic.itcast.cn.heima.demo08_BroadcastReceiver.orderedbroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyBroadcastReceiver02 extends BroadcastReceiver {
    public MyBroadcastReceiver02() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("MyBroadcastReceiver02","02接收到广播");
    }
}
