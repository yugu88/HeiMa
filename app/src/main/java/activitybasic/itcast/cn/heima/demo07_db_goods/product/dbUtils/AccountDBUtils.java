package activitybasic.itcast.cn.heima.demo07_db_goods.product.dbUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import activitybasic.itcast.cn.heima.demo07_db_goods.product.javabeen.Account;

/**
 * Created by qihao on 2016/10/20.17:08
 * for: HeiMa.
 */

public class AccountDBUtils {
    private MyHelper helper;

    public AccountDBUtils(Context context) {
        this.helper=new MyHelper(context);//创建数据库
    }
    public void insert(Account account){
        SQLiteDatabase db=helper.getWritableDatabase();//获取读写权限的数据库
        /**
         * ContentValues其实就是一个Map，Key值是字段名称，Value值是字段的值
         * contenvalues Key只能是String类型，values只能存储基本类型的数据
         */
        ContentValues values=new ContentValues();//ContentValues 常用在数据库中的操作
        values.put("name",account.getName());
        values.put("balance",account.getBalance());
        //
        long id=db.insert("account",null,values);//加入数据库时返回位置ID
        account.setId(id);
        db.close();
    }
    public int delete(long id){
        SQLiteDatabase db=helper.getWritableDatabase();
        int count=db.delete("account","_id=?",new String[]{id+""});
        db.close();
        return count;
    }
    public int update(Account account){
        SQLiteDatabase db=helper.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("name",account.getName());
        values.put("balance",account.getBalance());
        int count=db.update("account",values,"_id=?",new String[]{account.getId()+""});
        db.close();
        return count;
    }
    public List<Account> queryAll(){
        SQLiteDatabase db=helper.getReadableDatabase();
        Cursor cursor=db.query("account",null,null,null,null,null,"balance DESC");
        List<Account> list=new ArrayList<Account>();
        while (cursor.moveToNext()){
            long id=cursor.getLong(cursor.getColumnIndex("_id"));
            String name=cursor.getString(1);
            int balance=cursor.getInt(2);
            list.add(new Account(id,name,balance));
        }
        cursor.close();
        db.close();
        return list;
    }
}
