package activitybasic.itcast.cn.heima.demo08_BroadcastReceiver.ipdail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/**
 <!-- android:enabled 定义系统是否能够实例化这个广播接收器 -->
 <!-- android:exported 指示该广播接收器是否能够接收来自应用程序外部的消息 -->
 */
public class OutCallReceiver extends BroadcastReceiver {
    /**
     * 注册了一个受保护的intent,只能由系统发送。
     * 一个外向呼叫将被放置。
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        // 获取拨打的电话号码
        String outcallnumber=getResultData();
        // 创建SharedPreferences对象,获取该对象中存储的IP号码
        SharedPreferences sp=context.getSharedPreferences("config", Context.MODE_PRIVATE);
        String ipnumber=sp.getString("ipnumber", "");
        // 将IP号码添加到外拨电话的前面
        setResultData(ipnumber + outcallnumber);
    }
}
