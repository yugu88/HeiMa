package activitybasic.itcast.cn.heima.demo10_service_example.musicplayer;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

public class MediaService extends Service {

    private static final String TAG="MusicSercice";
    public MediaPlayer mediaPlayer;

    class MyBinder extends Binder {
        //播放音乐
        public void plays(String path) {
            play(path);
        }

        //暂停播放
        public void pauses() {
            pause();
        }

        //重新播放
        public void replays(String path) {
            replay(path);
        }

        //停止播放
        public void stops() {
            stop();
        }

        //获取当前播放进度
        public int getCurrentPosition() {
            return getCurrentProgress();
        }

        //获取音乐文件的长度
        public int getMusicWidth() {
            return getMusicLength();
        }
    }

    private int getMusicLength() {
        if (mediaPlayer != null) {
            return mediaPlayer.getDuration();
        }
        return 0;
    }

    private int getCurrentProgress() {
        if (mediaPlayer != null & mediaPlayer.isPlaying()) {
            Log.i(TAG, "获取当前进度");
            return mediaPlayer.getCurrentPosition();
        } else if (mediaPlayer != null & (!mediaPlayer.isPlaying())) {
            return mediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    private void stop() {
        Log.i(TAG, "停止播放");
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer=null;
        } else {
            Toast.makeText(getApplicationContext(), "已停止", Toast.LENGTH_SHORT).show();
        }

    }

    private void replay(String path) {
        if (mediaPlayer != null) {
            Log.i(TAG, "重新播放");
            try {
                mediaPlayer.seekTo(0);
                mediaPlayer.prepare();
                mediaPlayer.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void pause() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            Log.i(TAG, "播放暂停");
            mediaPlayer.pause();
        } else if (mediaPlayer != null && (!mediaPlayer.isPlaying())) {
            mediaPlayer.start();
        }
    }

    @SuppressLint("NewApi")
    private void play(String path) {
        try {
            if (mediaPlayer == null) {
                Log.i(TAG, "开始播放");
                mediaPlayer=new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(path);
                mediaPlayer.prepare();
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mediaPlayer.start();
                    }
                });
            } else {
                int position=getCurrentProgress();
                mediaPlayer.seekTo(position);
                mediaPlayer.prepare();
                mediaPlayer.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        if (mediaPlayer!=null){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer=null;
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }
    /**
     * setAudioSteamType(): 指定音频文件的类型必须在prepare()方法之前调用
     * setDataSource(): 设置要播放的音频文件的位置
     * prepare(): 再开始播放之前调用这个方法完成准备工作
     * start(): 开始或者继续播放音频
     * pause(): 暂停播放音频
     * reset(): 将MediaPlayer对象重置到刚刚创建的状态
     * seekTo(): 从指定的位置开始播放音频
     * stop(): 停止播放音频，调用该方法后MediaPlayer对象将无法再播放音频
     * release(): 释放掉与mediaPlayer对象相关的资源
     * isPlaying(): 判断当前MediaPlayer是否正在播放音频
     * getDuration(): 获取载入音频文件的时长
     * getCurrentPosition(): 获取当前播放音频文件的位置
     */
}
