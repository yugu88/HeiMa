package activitybasic.itcast.cn.heima.demo07_db_goods.product.javabeen;

/**
 * Created by qihao on 2016/10/20.17:04
 * for: HeiMa.
 */

public class Account {
    private long id;
    private String name;
    private Integer balance;//称重量

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id=id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name=name;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance=balance;
    }

    public Account(long id, String name, Integer balance) {
        super();
        this.id=id;
        this.name=name;
        this.balance=balance;
    }

    public Account(String name, Integer balance) {
        super();
        this.name=name;
        this.balance=balance;
    }

    public Account() {
        super();
    }

    @Override
    public String toString() {
        return "[序号："+id+"，商品名称："+name+"，余额："+balance+"]";
    }
}
