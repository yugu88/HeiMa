package activitybasic.itcast.cn.heima.demo08_BroadcastReceiver.ipdail;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import activitybasic.itcast.cn.heima.R;

public class Main11Activity extends AppCompatActivity {

    private EditText et_ipnumber;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main11);

        et_ipnumber=(EditText) findViewById(R.id.et_ipnumber);
        sp=getSharedPreferences("config",MODE_PRIVATE);
        et_ipnumber.setText(sp.getString("ipnumber",""));
    }

    public void click(View view) {
        String ipnumber=et_ipnumber.getText().toString().trim();
        SharedPreferences.Editor editor=sp.edit();
        editor.putString("ipnumber",ipnumber);
        editor.commit();
        Toast.makeText(Main11Activity.this,"设置成功",Toast.LENGTH_SHORT).show();
    }

}
