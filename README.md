# HeiMa
黑马Android开发 
随书所有案例，抛弃书中eclipse，使用最新版studio和SDK重新编写

|HeiMa|更新 | 版本 |
|------------|-----------|--------|
Android 案例 | Markdown| 1.0.0 |

#### Log日志控制
* MyAppAplication 
    - MLog.init(true);// Log日志控制
    
#### 编译版本
    compileSdkVersion 25
    buildToolsVersion "25.0.3"
    defaultConfig {
        applicationId "activitybasic.itcast.cn.heima"
        minSdkVersion 14
        targetSdkVersion 25
        versionCode 1
        versionName "1.0"
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
    }
#### gradle版本   
    dependencies {
         classpath 'com.android.tools.build:gradle:2.3.3'
         // NOTE: Do not place your application dependencies here; they belong
         // in the individual module build.gradle files
     }
    
#### settings依赖
    include ':app'
    
#### gradle-wrapper.properties
    distributionUrl=https\://services.gradle.org/distributions/gradle-3.3-all.zip
    
#### 关于退出和栈中Activity实时管理
    ```
        /**
         * 作为良心开发者，不能因为用户多安装一个app而增加一丝卡顿，app退出时必须调用此方法。
         * 彻底退出应用，清空相关的所有进程和堆栈内存
         * 注意：可用内存查看工具查看app退出后是否在堆栈中被彻底清除
         */
            public void AppExit();
        // 栈中移除其他Activity
            ActivityUtil.getInstance().popOtherActivity(MainActivity.class);
        // 把当前Activity添加进管理集合
            ActivityUtil.getInstance().addActivity(this);
            
            
       /**
        * 并非每次从栈中移除都节省CPU和内存的消耗，根据跳转逻辑斟酌使用。
        */
    ```
    
